import React from 'react';
import Food from '../food';

let local;
if (localStorage.getItem('alim') == null) {
  localStorage.setItem('alim', JSON.stringify([]));
}
local = localStorage.getItem('alim');

export default function Bevande(props) {
  return (
    <div>
      <div>
        {props.bevande.map((bev) => (
          <Food categoria="Bevande" prezzo={bev.prezzo} nome={bev.nome} />
        ))}
      </div>
    </div>
  );
}
