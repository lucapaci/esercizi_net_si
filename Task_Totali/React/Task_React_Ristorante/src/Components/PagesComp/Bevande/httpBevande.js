import React, { useState } from 'react';
import Bevande from './bevande';

export default function HttpBevande() {
  const [bevande, setBevande] = useState([]);

  fetch('http://localhost:4001/api/foods/category/62628673769e9f697841afa0')
    .then((resp) => resp.json())
    .then((resp) => {
      const bevandeTrasformate = resp.map((bevan) => {
        return {
          nome: bevan.nome,
          prezzo: bevan.prezzo,
        };
      });
      setBevande(bevandeTrasformate);
    });

  return (
    <div>
      <Bevande bevande={bevande} />
    </div>
  );
}
