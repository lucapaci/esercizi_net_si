import React from 'react';

export default function Food(props) {
  return (
    <div>
      <div className="card">
        <h5 className="card-header">{props.categoria}</h5>
        <div className="card-body">
          <h5 className="card-title">{props.nome}</h5>
          <p className="card-text">Prezzo: {props.prezzo} Euro</p>
          <button className="btn btn-outline-primary btn-sm">
            Aggiungi al carrello
          </button>
        </div>
      </div>
    </div>
  );
}
