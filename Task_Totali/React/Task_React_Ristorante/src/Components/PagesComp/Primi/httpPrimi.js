import React, { useState } from 'react';
import Primi from './primi';

export default function HttpPrimi() {
  const [primi, setPrimi] = useState([]);

  fetch('http://localhost:4001/api/foods/category/6262863f769e9f697841af9f')
    .then((resp) => resp.json())
    .then((resp) => {
      const primiTrasformati = resp.map((piatti) => {
        return {
          nome: piatti.nome,
          prezzo: piatti.prezzo,
        };
      });
      setPrimi(primiTrasformati);
    });

  return (
    <div>
      <Primi primi={primi} />
    </div>
  );
}
