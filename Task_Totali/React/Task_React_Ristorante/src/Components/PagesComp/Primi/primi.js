import React from 'react';
import Food from '../food';

let local;
if (localStorage.getItem('alim') == null) {
  localStorage.setItem('alim', JSON.stringify([]));
}
local = localStorage.getItem('alim');

export default function Primi(props) {
  return (
    <div>
      <div>
        {props.primi.map((cibo) => (
          <Food categoria="Primi" prezzo={cibo.prezzo} nome={cibo.nome} />
        ))}
      </div>
    </div>
  );
}
