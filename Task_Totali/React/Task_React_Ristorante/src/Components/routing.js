import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './header/header';
import HttpBevande from './PagesComp/Bevande/httpBevande';
import HttpPrimi from './PagesComp/Primi/httpPrimi';
//import Primi from './PagesComp/Primi/primi';

export default function Routing() {
  return (
    <div>
      <Header />
      <main>
        <Routes>
          <Route path="/primi" element={<HttpPrimi />} />
          <Route path="/bevande" element={<HttpBevande />} />
        </Routes>
      </main>
    </div>
  );
}
