const express = require('express');
const mongoose = require('mongoose');
const Category = require('./models/Category');
const Food = require('./models/Food');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extender: true }));
app.use(cors());

const porta = 4001;
const dbName = 'Ristorante';

const db = mongoose.connect(
  'mongodb://localhost:27017/' + dbName,
  { useNewUrlParser: true },
  () => {
    console.log('Sono connesso!');
  }
);

app.listen(porta, () => {
  console.log(`Sto ascoltando alla porta: ${porta}`);
});

app.get('/api/foods', (req, res) => {
  Food.find({})
    .populate('categories')
    .exec((err, elenco) => {
      if (!err) res.json(elenco);
      else res.status(500);
    });
});

app.get('/api/foods/category/:idCategory', (req, res) => {
  Food.find({}, (err, elenco) => {
    let filteredArray = [];
    elenco.forEach((i) => {
      if (i.categories.includes(req.params.idCategory)) filteredArray.push(i);
    });
    if (!err) res.json(filteredArray);
    else res.status(500);
  });
});

app.get('/api/foods/:idfood', (req, res) => {
  Food.find({ _id: req.params.idfood })
    .populate('categories')
    .exec((err, item) => {
      if (!err) res.json(item);
      else res.status(500);
    });
});

app.post('/api/foods/insert', async (req, res) => {
  let food = await Food.create(req.body);
  if (food) res.json(food);
  else res.status(500).end();
});

app.delete('/api/foods/:idfood', (req, res) => {
  Food.findByIdAndDelete(req.params.idfood, (err, item) => {
    if (!err) res.json(item);
    else res.status(500);
  });
});

app.put('/api/foods/:idfood', (req, res) => {
  Food.findByIdAndUpdate(req.params.idfood, req.body, (err, item) => {
    if (!err) res.status(200).json({ status: 'success' });
    else res.status(500).end();
  });
});

app.get('/api/categories', (req, res) => {
  Category.find({}, (err, elenco) => {
    if (!err) res.json(elenco);
    else res.status(500);
  });
});

app.get('/api/categories/:idcategory', (req, res) => {
  Category.findById(req.params.idcategory, (err, item) => {
    if (!err) res.status(200).json(item);
    else res.status(500).end();
  });
});

app.post('/api/categories/insert', async (req, res) => {
  let category = await Category.create(req.body);

  if (category) res.json(category);
  else res.status(500).end();
});

app.get('/api/food/categories/:idfood', (req, res) => {
  Food.findById(req.params.idfood, (err, item) => {
    if (!err) {
      Category.where('_id')
        .in(item.categories)
        .select('nome')
        .exec((err, categories) => {
          if (!err) res.status(200).json(categories);
          else res.status(500).end();
        });
    } else {
      res.status(500).end();
    }
  });
});
