const mongoose = require('mongoose');
const Category = require('./Category');
const Schema = mongoose.Schema;
const FoodSchema = new Schema({
  nome: String,
  prezzo: Number,
  categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
});

const Food = mongoose.model('Food', FoodSchema);

module.exports = Food;
