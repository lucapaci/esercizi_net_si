﻿using System;
using System.Collections.Generic;

#nullable disable

namespace API_Chat_Task.Models
{
    public partial class Messaggi
    {
        public int MessaggioId { get; set; }
        public string Nickname { get; set; }
        public string Orario { get; set; }
        public string Contenuto { get; set; }
    }
}
