﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Chat_Task.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Chat_Task.Controllers
{   
    [Route("api/messaggi")]
    [ApiController]
    public class MessaggiController : Controller
    {
        private readonly db_chatContext _context = new db_chatContext();

        [HttpGet]
        public ActionResult<IEnumerable<Messaggi>> GetAllMessaggiAsync() => Ok(_context.Messaggis.ToListAsync().Result);

        [HttpGet("{varId}")]
        public ActionResult<Messaggi> GetMessaggioByIdAsync(int varId) =>
            Ok(_context.Messaggis.SingleOrDefaultAsync(mes => mes.MessaggioId == varId).Result);

        [HttpPost("insert")]
        public ActionResult InsertMessaggioAsync(Messaggi obj)
        {
            _context.Add(obj);
            if (_context.SaveChangesAsync().Result > 0)
            {
                return Ok("Successo");
            }

            return Ok("Problemi nell'inserimento");
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyMessaggio(Messaggi msg, int varId)
        {
             Messaggi temp = _context.Messaggis.FirstOrDefaultAsync(i => i.MessaggioId == varId).Result;
             temp.Contenuto = msg.Contenuto;
             temp.Orario = msg.Orario;
             temp.Nickname = msg.Nickname;
             if (_context.SaveChangesAsync().Result > 0)
             {
                 return Ok("Successo");
             }

             return Ok("Problemi nella modifica");
        }

        [HttpDelete("{varId}")]
        public ActionResult DeleteMessaggio(int varId)
        {
            _context.Messaggis.Remove(_context.Messaggis.FirstOrDefaultAsync(i => i.MessaggioId == varId).Result);
            if (_context.SaveChangesAsync().Result > 0)
            {
                return Ok("Successo");
            }

            return Ok("Problemi nell'eliminazione");
        }
    }
}
