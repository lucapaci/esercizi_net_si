﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Models;

namespace ASP_Palestra_Task.Data
{
    public interface IUtenteRepository
    {
        IEnumerable<Utente> GetAll();

        Utente GetById(int varId);

        Utente Insert(Utente t);

        Utente Delete(Utente t);

        Utente GetByUsername(string username);
    }
}
