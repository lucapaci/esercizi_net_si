﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Models;

namespace ASP_Palestra_Task.Data
{
    public interface IIscrizioneRepository
    {
        IEnumerable<Iscrizione> GetAll();

        Iscrizione GetById(int varId);

        IEnumerable<Corso> GetCorsiByUser(int varId);

        Iscrizione Insert(Iscrizione t);

        Iscrizione Delete(Iscrizione t);

        IEnumerable<Corso> GetAllCorsi();

        Corso GetCorsoById(int varId);
    }
}
