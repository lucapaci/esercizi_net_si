#pragma checksum "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0b983867f57f0b8571d582186b5151f350869faf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Iscrizione_AreaPersonale), @"mvc.1.0.view", @"/Views/Iscrizione/AreaPersonale.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
using ASP_Palestra_Task.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0b983867f57f0b8571d582186b5151f350869faf", @"/Views/Iscrizione/AreaPersonale.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b853c2693020103458b52d8246f923ca3fd89014", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Iscrizione_AreaPersonale : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ASP_Palestra_Task.ViewModels.IscrizioneViewModel>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-outline-danger btn-sm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Iscrizione", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Cancella", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
  
    Layout = "_Layout";
    ViewData["Title"] = "Area Personale";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col-md-4 mt-5 bg-dark rounded text-white p-3\">\r\n        <h4>Username(Email):</h4>\r\n            <h6>");
#nullable restore
#line 11 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
           Write(Model.UtenteLogged.Username);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n            <h4 class=\"text\">Nome:</h4>\r\n            <h6>");
#nullable restore
#line 13 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
           Write(Model.UtenteLogged.Nome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n            <h4>Cognome:</h4>\r\n            <h6>");
#nullable restore
#line 15 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
           Write(Model.UtenteLogged.Cognome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n             <h4>Indirizzo:</h4>\r\n            <h6>");
#nullable restore
#line 17 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
           Write(Model.UtenteLogged.Indirizzo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n            <h4>Data di nascita:</h4>\r\n            <h6>");
#nullable restore
#line 19 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
           Write(Model.UtenteLogged.DataNascita.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n    </div>\r\n    <div class=\"col-md-8 mt-5\">\r\n");
#nullable restore
#line 22 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
         if (Model.Iscrizioni.Count != 0)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"list-group scrollbar scrollbar-info\">\r\n                <div class=\"force-overflow\">\r\n");
#nullable restore
#line 26 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
                     foreach (Iscrizione item in Model.Iscrizioni)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <div class=\"list-group-item bg-dark text-white\">\r\n                            <div class=\"d-flex w-100 justify-content-between mb-2\">\r\n                                <h4>");
#nullable restore
#line 30 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
                               Write(item.CorsoRifNavigation.Titolo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h4>\r\n                                <small class=\"text-muted\">");
#nullable restore
#line 31 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
                                                     Write(item.DataIscrizione.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</small>\r\n                            </div>\r\n                            <p class=\"mb-1\">");
#nullable restore
#line 33 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
                                       Write(item.CorsoRifNavigation.Descrizione);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                            <div class=\"text-right\">\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0b983867f57f0b8571d582186b5151f350869faf8533", async() => {
                WriteLiteral("Annulla Iscrizione");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 35 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
                                                                                                                             WriteLiteral(item.IscrizioneID);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                            </div>\r\n                        </div>\r\n");
#nullable restore
#line 38 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </div>\r\n            </div>\r\n");
#nullable restore
#line 41 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
        }
        else
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-dark text-center\">\r\n                <h3>Non sei iscritto a nessun corso!</h3>\r\n            </div>\r\n");
#nullable restore
#line 47 "C:\Users\hp\Desktop\Programmazione\FormaTemp\Lezioni\ASP\ASP_Palestra_Task\ASP_Palestra_Task\Views\Iscrizione\AreaPersonale.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ASP_Palestra_Task.ViewModels.IscrizioneViewModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
