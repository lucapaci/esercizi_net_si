﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Data;
using ASP_Palestra_Task.Models;
using ASP_Palestra_Task.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ASP_Palestra_Task.Controllers
{
    public class IscrizioneController : Controller
    {
        private readonly IIscrizioneRepository _repoIscr;
        private readonly IUtenteRepository _repoUser;

        public IscrizioneController(IIscrizioneRepository repoIscr, IUtenteRepository repoUser)
        {
            _repoUser = repoUser;
            _repoIscr = repoIscr;
        }

        [HttpGet]
        public IActionResult IscrizioneCorso()
        {
            string username = HttpContext.Session.GetString("IsLogged");
            ViewData["User"] = username;
            if (username == null)
                return Redirect("/Home/LogIn");

            Utente loggato = _repoUser.GetByUsername(username);
            List<Corso> corsi = _repoIscr.GetAllCorsi().ToList();

            _repoIscr.GetCorsiByUser(loggato.UtenteId).ToList().ForEach(i =>
            {
                if (corsi.Contains(i)) corsi.Remove(i);
            });
            return View(new IscrizioneViewModel { Corsi = corsi, UtenteLogged = loggato });
        }

        
        public IActionResult Iscrizione([FromRoute]int id)
        {
            string username = HttpContext.Session.GetString("IsLogged");
            ViewData["User"] = username;
            if (username == null)
                return Redirect("/Home/LogIn");

            Utente loggato = _repoUser.GetByUsername(username);
            Iscrizione iscriz = new Iscrizione
            {
                CorsoRif = id,
                UtenteRif = loggato.UtenteId
            };
            _repoIscr.Insert(iscriz);

            return RedirectToAction("IscrizioneCorso");
        }

        [HttpGet]
        public IActionResult AreaPersonale()
        {
            string username = HttpContext.Session.GetString("IsLogged");
            ViewData["User"] = username;
            if (username == null)
                return Redirect("/Home/LogIn");

            Utente loggato = _repoUser.GetByUsername(username);
            List<Iscrizione> iscrizioni = _repoIscr.GetAll().Where(i => i.UtenteRif == loggato.UtenteId).ToList();

            return View(new IscrizioneViewModel { Iscrizioni = iscrizioni, UtenteLogged = loggato });
        }

        public IActionResult Cancella([FromRoute]int id)
        {
            string username = HttpContext.Session.GetString("IsLogged");
            ViewData["User"] = username;
            if (username == null)
                return Redirect("/Home/LogIn");

            Iscrizione iscr = _repoIscr.GetById(id);
            _repoIscr.Delete(iscr);
            return RedirectToAction("AreaPersonale");
        }
    }
}
