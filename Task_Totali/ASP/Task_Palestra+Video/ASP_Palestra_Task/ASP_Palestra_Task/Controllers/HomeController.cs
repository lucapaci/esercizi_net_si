﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Data;
using ASP_Palestra_Task.Models;
using ASP_Palestra_Task.ViewModels;
using Microsoft.AspNetCore.Http;

namespace ASP_Palestra_Task.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUtenteRepository _repoUtenti;
        private readonly IIscrizioneRepository _repoIscr;

        public HomeController(IUtenteRepository repoUtenti, IIscrizioneRepository repoIscr)
        {
            _repoUtenti = repoUtenti;
            _repoIscr = repoIscr;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["User"] = HttpContext.Session.GetString("IsLogged");
            return View(new IscrizioneViewModel{Corsi = _repoIscr.GetAllCorsi().ToList()});
        }

        [HttpGet]
        public IActionResult LogIn()
        {
            return View(new Credenziali());
        }
        
        [HttpPost]
        public IActionResult LogIn([FromForm] Credenziali cred)
        {
            Utente user = _repoUtenti.GetByUsername(cred.Username);
            if (user != null && cred.Password == user.PasswordUtente)
            {
                HttpContext.Session.SetString("IsLogged", cred.Username);
                ViewData["User"] = cred.Username;
                return RedirectToAction("Index");
            }

            return View(cred);
        }
        
        [HttpGet]
        public IActionResult SignIn()
        {
            return View(new Utente());
        }

        [HttpPost]
        public IActionResult SignIn([FromForm] Utente user)
        {
            Utente temp = _repoUtenti.GetByUsername(user.Username);
            if(ModelState.IsValid && 
               temp == null &&
               user.PasswordUtente.Equals(user.PasswordConferma))
            {
                _repoUtenti.Insert(user);
                return RedirectToAction("LogIn");
            }

            return View(user);
        }

        public IActionResult LogOut()
        {
            HttpContext.Session.Remove("IsLogged");
            return RedirectToAction("Index");
        }

    }
}
