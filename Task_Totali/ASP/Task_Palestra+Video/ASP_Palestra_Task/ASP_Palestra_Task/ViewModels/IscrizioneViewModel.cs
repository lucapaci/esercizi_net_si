﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ASP_Palestra_Task.ViewModels
{
    public class IscrizioneViewModel
    {
        public Utente UtenteLogged { get; set; }

        public List<Corso> Corsi { get; set; }

        public Iscrizione Iscrizione { get; set; }

        public List<Iscrizione> Iscrizioni { get; set; }
    }
}
