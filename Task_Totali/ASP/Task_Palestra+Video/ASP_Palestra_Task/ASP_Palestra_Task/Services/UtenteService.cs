﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Data;
using ASP_Palestra_Task.Models;

namespace ASP_Palestra_Task.Services
{
    public class UtenteService : IUtenteRepository
    {
        private readonly db_PalestraASPContext _context;

        public UtenteService(db_PalestraASPContext context)
        {
            _context = context;
        }

        public IEnumerable<Utente> GetAll() => _context.Utenti.ToList();

        public Utente GetById(int varId) => _context.Utenti.FirstOrDefault(i => i.UtenteId == varId);

        public Utente Insert(Utente t)
        {
            Utente temp = _context.Utenti.FirstOrDefault(i => i.Username == t.Username);
            if (temp == null)
            {
                _context.Utenti.Add(t);
                _context.SaveChanges();
                return t;
            }

            return null;
        }

        public Utente Delete(Utente t)
        {
            Utente temp = _context.Utenti.FirstOrDefault(i => i.UtenteId == t.UtenteId);
            if (temp != null)
            {
                _context.Utenti.Remove(t);
                _context.SaveChanges();
                return t;
            }

            return null;
        }

        public Utente GetByUsername(string username) => _context.Utenti.FirstOrDefault(i => i.Username.Equals(username));
    }
}
