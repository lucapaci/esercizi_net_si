﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_Palestra_Task.Data;
using ASP_Palestra_Task.Models;

namespace ASP_Palestra_Task.Services
{
    public class IscrizioneService : IIscrizioneRepository
    {
        private readonly db_PalestraASPContext _context;

        public IscrizioneService(db_PalestraASPContext context)
        {
            _context = context;
        }

        public IEnumerable<Iscrizione> GetAll()
        {
            List<Iscrizione> iscritti = _context.Iscrizioni.ToList();
            iscritti.ForEach(i =>
            {
                i.CorsoRifNavigation = GetCorsoById(i.CorsoRif);
                i.UtenteRifNavigation = _context.Utenti.FirstOrDefault(el => el.UtenteId == i.UtenteRif);
            });
            return iscritti;
        }

        public Iscrizione GetById(int varId) => _context.Iscrizioni.FirstOrDefault(i => i.IscrizioneID == varId);

        public IEnumerable<Corso> GetCorsiByUser(int varId)
        {
             List<Iscrizione> iscrizioni = _context.Iscrizioni.Where(i => i.UtenteRif == varId).ToList();
             if (iscrizioni.Count == 0)
                 return new List<Corso>();

             return iscrizioni.Select(i => GetCorsoById(i.CorsoRif)).ToList();

        }


        public Iscrizione Insert(Iscrizione t)
        {
            Iscrizione temp =
                _context.Iscrizioni.FirstOrDefault(i => i.UtenteRif == t.UtenteRif && i.CorsoRif == t.CorsoRif);

            if (temp == null && t != null)
            {
                _context.Iscrizioni.Add(t);
                _context.SaveChanges();
                return t;
            }

            return null;
        }

        public Iscrizione Delete(Iscrizione t)
        {
            Iscrizione temp =
                _context.Iscrizioni.FirstOrDefault(i => i.UtenteRif == t.UtenteRif && i.CorsoRif == t.CorsoRif);

            if (temp != null && t != null)
            {
                _context.Iscrizioni.Remove(t);
                _context.SaveChanges();
                return t;
            }

            return null;
        }

        public IEnumerable<Corso> GetAllCorsi() => _context.Corsi.ToList();

        public Corso GetCorsoById(int varId) => _context.Corsi.FirstOrDefault(i => i.CorsoId == varId);
    }
}
