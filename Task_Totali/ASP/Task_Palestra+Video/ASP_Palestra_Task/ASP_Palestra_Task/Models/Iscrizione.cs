﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace ASP_Palestra_Task.Models
{
    public partial class Iscrizione
    {
        [Key]
        public int IscrizioneID { get; set; }

        public DateTime DataIscrizione { get; set; }
        [ForeignKey("corsoRIF")]
        public int CorsoRif { get; set; }
        [ForeignKey("utenteRIF")]
        public int UtenteRif { get; set; }

        public virtual Corso CorsoRifNavigation { get; set; }
        public virtual Utente UtenteRifNavigation { get; set; }

        public Iscrizione()
        {
            DataIscrizione = DateTime.Now;
        }
    }
}
