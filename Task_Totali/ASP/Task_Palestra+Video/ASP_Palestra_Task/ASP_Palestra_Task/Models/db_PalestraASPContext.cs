﻿using System;
using ASP_Palestra_Task.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ASP_Palestra_Task.Models
{
    public partial class db_PalestraASPContext : DbContext
    {
        public db_PalestraASPContext()
        {
        }

        public db_PalestraASPContext(DbContextOptions<db_PalestraASPContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Corso> Corsi { get; set; }
        public virtual DbSet<Iscrizione> Iscrizioni { get; set; }
        public virtual DbSet<Utente> Utenti { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=LAPTOP-V2JOQPIF;Database=db_PalestraASP;User Id=sharpuser;Password=cicciopasticcio;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Corso>(entity =>
            {
                entity.HasKey(e => e.CorsoId)
                    .HasName("PK__Corsi__064A9549C372FAD2");

                entity.ToTable("Corsi");

                entity.Property(e => e.CorsoId).HasColumnName("corsoID");

                entity.Property(e => e.Codice)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("codice");

                entity.Property(e => e.DataFine)
                    .HasColumnType("date")
                    .HasColumnName("dataFine");

                entity.Property(e => e.DataInizio)
                    .HasColumnType("date")
                    .HasColumnName("dataInizio");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("descrizione");

                entity.Property(e => e.OraFine)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("oraFIne");

                entity.Property(e => e.OraInizio)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("oraInizio");

                entity.Property(e => e.Titolo)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("titolo");
            });

            modelBuilder.Entity<Iscrizione>(entity =>
            {
                entity.HasKey(e => e.IscrizioneID);

                entity.ToTable("Iscrizioni");

                entity.HasIndex(e => new { e.CorsoRif, e.UtenteRif }, "UQ__Iscrizio__B761C90F655F7397")
                    .IsUnique();

                entity.Property(e => e.CorsoRif).HasColumnName("corsoRIF");

                entity.Property(e => e.DataIscrizione)
                    .HasColumnType("date")
                    .HasColumnName("dataIscrizione");

                entity.Property(e => e.UtenteRif).HasColumnName("utenteRIF");

                entity.HasOne(d => d.CorsoRifNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.CorsoRif)
                    .HasConstraintName("FK__Iscrizion__corso__29572725");

                entity.HasOne(d => d.UtenteRifNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.UtenteRif)
                    .HasConstraintName("FK__Iscrizion__utent__2A4B4B5E");
            });

            modelBuilder.Entity<Utente>(entity =>
            {
                entity.HasKey(e => e.UtenteId)
                    .HasName("PK__Utenti__CA5C2253579E3367");

                entity.ToTable("Utenti");

                entity.HasIndex(e => e.Username, "UQ__Utenti__F3DBC57270E47FAA")
                    .IsUnique();

                entity.Property(e => e.DataNascita).HasColumnName("dataNascita");

                entity.Property(e => e.UtenteId).HasColumnName("utenteID");

                entity.Property(e => e.Cognome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cognome");

                entity.Property(e => e.Indirizzo)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("indirizzo");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nome");

                entity.Property(e => e.PasswordUtente)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("passwordUtente");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("username");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
