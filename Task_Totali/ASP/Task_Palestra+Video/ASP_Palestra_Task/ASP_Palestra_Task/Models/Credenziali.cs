﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Palestra_Task.Models
{
    public class Credenziali
    {
        [Required(ErrorMessage = "Campo richiesto")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Campo richiesto")]
        public string Password { get; set; }

    }
}
