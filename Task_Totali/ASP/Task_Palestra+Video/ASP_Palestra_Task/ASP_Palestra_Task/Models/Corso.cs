﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace ASP_Palestra_Task.Models
{
    public partial class Corso
    {
        [Key]
        public int CorsoId { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MaxLength(200, ErrorMessage = "Il campo è troppo lungo"),
         MinLength(3, ErrorMessage = "Il campo è troppo corto")]
        public string Titolo { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MinLength(5, ErrorMessage = "Il campo è troppo corto")]
        public string Descrizione { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio")]
        public string Codice { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio")]
        public DateTime DataInizio { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio")]
        public DateTime DataFine { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MinLength(4, ErrorMessage = "Data non Valida"),
         MaxLength(5, ErrorMessage = "Data non valida")]
        public string OraInizio { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MinLength(4, ErrorMessage = "Data non Valida"),
         MaxLength(5, ErrorMessage = "Data non valida")]
        public string OraFine { get; set; }
    }
}
