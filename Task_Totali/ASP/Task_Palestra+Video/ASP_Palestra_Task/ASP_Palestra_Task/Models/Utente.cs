﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace ASP_Palestra_Task.Models
{
    public partial class Utente
    {
        [Key]
        public int UtenteId { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MaxLength(50, ErrorMessage = "Il campo è troppo lungo"),
         MinLength(2, ErrorMessage = "Il campo è troppo corto")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MaxLength(50, ErrorMessage = "Il campo è troppo lungo"),
         MinLength(3, ErrorMessage = "Il campo è troppo corto")]
        public string Cognome { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MaxLength(200, ErrorMessage = "Il campo è troppo lungo"),
         MinLength(5, ErrorMessage = "Il campo è troppo corto")]
        public string Indirizzo { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio")]
        public DateTime DataNascita { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MaxLength(300, ErrorMessage = "Il campo è troppo lungo"),
         MinLength(7, ErrorMessage = "Il campo è troppo corto"),
         RegularExpression(@"^[^@\s]+@[^@\s]+\.[^@\s]+$", ErrorMessage = "Email non valida")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         MaxLength(300, ErrorMessage = "Il campo è troppo lungo"),
         MinLength(3, ErrorMessage = "Password troppo corta")]
        public string PasswordUtente { get; set; }
        [Required(ErrorMessage = "Il campo è obbligatorio"),
         NotMapped]
        public string PasswordConferma { get; set; }
    }
}
