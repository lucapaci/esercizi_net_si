﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace API_Libri_CodeFirst.Models
{
    
    [Index(nameof(Codice), IsUnique = true)]
    public class Libro
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(200), Required]
        public string Nome { get; set; }
        [MaxLength(200), Required]
        public string Descrizione { get; set; }
        [MaxLength(200), Required]
        public string Codice { get; set; }
        [Required]
        public int Quantita { get; set; }
        [Required]
        public Categoria Categoria { get; set; }
    }
}
