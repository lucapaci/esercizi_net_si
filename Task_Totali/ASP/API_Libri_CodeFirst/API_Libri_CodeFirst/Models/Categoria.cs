﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace API_Libri_CodeFirst.Models
{
    [Table("Categorie")]
    public class Categoria
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(150), Required]
        public string Titolo { get; set; }
        [MaxLength(100), Required]
        public string Scaffale { get; set; }
        [MaxLength(200), Required]
        public string Descrizione { get; set; }
        [MaxLength(36)]
        public string Codice { get; set; }

        public string GeneraCodice()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
