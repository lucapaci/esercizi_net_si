﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Libri_CodeFirst.Data
{
    public interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        bool Insert(T obj);
        bool Modify(int id, T obj);
        bool Delete(int id);
        T GetByCode(string code);
    }
}
