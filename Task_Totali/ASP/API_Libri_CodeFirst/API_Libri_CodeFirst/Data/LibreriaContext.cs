﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Libri_CodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace API_Libri_CodeFirst.Data
{
    public class LibreriaContext : DbContext
    {
        public LibreriaContext(DbContextOptions<LibreriaContext> options) : base(options)
        {
            
        }

        public DbSet<Libro> Libri { get; set; }
        public DbSet<Categoria> Categorie { get; set; }
    }
}
