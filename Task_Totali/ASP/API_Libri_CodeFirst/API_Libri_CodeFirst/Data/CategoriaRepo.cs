﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Libri_CodeFirst.Models;

namespace API_Libri_CodeFirst.Data
{
    public class CategoriaRepo : InterfaceRepo<Categoria>
    {
        private readonly LibreriaContext _context;

        public CategoriaRepo(LibreriaContext context)
        {
            _context = context;
        }

        public IEnumerable<Categoria> GetAll() =>  _context.Categorie.ToList();


        public Categoria GetById(int id) => _context.Categorie.FirstOrDefault(i => i.Id == id);

        public bool Insert(Categoria obj)
        {
            obj.Codice = obj.GeneraCodice();
            _context.Categorie.Add(obj);
            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }

        public bool Modify(int id, Categoria obj)
        {
            obj.Id = id;
            _context.Categorie.Update(obj);
            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }

        public bool Delete(int id)
        {
            Categoria temp = GetById(id);
            _context.Categorie.Remove(temp);
            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }

        public Categoria GetByCode(string code) =>
                _context.Categorie.FirstOrDefault(i => i.Codice.ToLower().Equals(code.ToLower()));
    }
}
