﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Libri_CodeFirst.Models;

namespace API_Libri_CodeFirst.Data
{
    public class LibriRepo : InterfaceRepo<Libro>
    {
        private readonly LibreriaContext _context;

        public LibriRepo(LibreriaContext context)
        {
            _context = context;
        }

        public IEnumerable<Libro> GetAll() => _context.Libri.ToList();
        
        public Libro GetById(int varId) => _context.Libri.FirstOrDefault(i => i.Id == varId);
        
        public bool Insert(Libro obj)
        {
            _context.Libri.Add(obj);
            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }

        public bool Modify(int varId, Libro obj)
        {
            obj.Id = varId;
            _context.Libri.Update(obj);
            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }

        public bool Delete(int varId)
        {
            Libro temp = GetById(varId);
            _context.Libri.Remove(temp);
            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }

        public Libro GetByCode(string varCode) => 
            _context.Libri.FirstOrDefault(i => i.Codice.ToLower().Equals(varCode.ToLower()));
        
    }
}
