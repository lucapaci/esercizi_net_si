﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Libri_CodeFirst.Migrations
{
    public partial class boh2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoriaId",
                table: "Libri",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Categoria",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titolo = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Scaffale = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Descrizione = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Codice = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categoria", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Libri_CategoriaId",
                table: "Libri",
                column: "CategoriaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Libri_Categoria_CategoriaId",
                table: "Libri",
                column: "CategoriaId",
                principalTable: "Categoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Libri_Categoria_CategoriaId",
                table: "Libri");

            migrationBuilder.DropTable(
                name: "Categoria");

            migrationBuilder.DropIndex(
                name: "IX_Libri_CategoriaId",
                table: "Libri");

            migrationBuilder.DropColumn(
                name: "CategoriaId",
                table: "Libri");
        }
    }
}
