﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Libri_CodeFirst.Data;
using API_Libri_CodeFirst.Models;
using Microsoft.AspNetCore.Mvc;

namespace API_Libri_CodeFirst.Controllers
{
    [Route("api/Categorie")]
    [ApiController]
    public class CategoriaController : Controller
    {
        private readonly InterfaceRepo<Categoria> _repo;

        public CategoriaController(InterfaceRepo<Categoria> repo) => _repo = repo;

        [HttpGet]
        public ActionResult<IEnumerable<Categoria>> GetAllCategorie() => Ok(_repo.GetAll());

        [HttpGet("{varId}")]
        public ActionResult<Categoria> GetCategoriaById(int varId) => Ok(_repo.GetById(varId));

        [HttpPost("insert")]
        public ActionResult InsertCategoria(Categoria obj)
        {
            if (_repo.Insert(obj))
                return Ok("success");
            
            return Ok("error");
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyCategoria(int varId, Categoria obj)
        {
            if (_repo.Modify(varId, obj))
                return Ok("success");
            return Ok("error");
        }
    }
}
