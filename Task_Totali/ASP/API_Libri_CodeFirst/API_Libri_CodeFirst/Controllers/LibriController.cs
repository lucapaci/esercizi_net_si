﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Libri_CodeFirst.Data;
using API_Libri_CodeFirst.Models;
using Microsoft.AspNetCore.Mvc;

namespace API_Libri_CodeFirst.Controllers
{
    [Route("api/libri")]
    [ApiController]
    public class LibriController : Controller
    {
        private readonly InterfaceRepo<Libro> _repo;

        public LibriController(InterfaceRepo<Libro> repo) => _repo = repo;
        

        [HttpGet]
        public ActionResult<IEnumerable<Libro>> GetAllLibri() => Ok(_repo.GetAll());

        [HttpGet("{varId}")]
        public ActionResult<Libro> GetLibroById(int varId) => Ok(_repo.GetById(varId));

        [HttpPost("insert")]
        public ActionResult InsertLibro(Libro obj)
        {
            Libro temp = _repo.GetByCode(obj.Codice);
            if (temp != null)
            {
                obj.Quantita += temp.Quantita;
                if (_repo.Modify(temp.Id, obj))
                    return Ok("success");
            }
            else
            {
                if (_repo.Insert(obj))
                    return Ok("success");
            }
            return Ok("error");
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyLibro(int varId, Libro obj)
        {
            if (_repo.Modify(varId, obj))
                return Ok("success");
            return Ok("error");
        }

        [HttpDelete("{varId}")]
        public ActionResult DeleteLibro(int varId)
        {
            if (_repo.Delete(varId))
                return Ok("success");
            return Ok("error");
        }
    }
}
