using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Libri_CodeFirst.Data;
using API_Libri_CodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace API_Libri_CodeFirst
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(req => req.AddPolicy(
                "policyLocale", builder => {
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                }
            ));

            services.AddDbContext<LibreriaContext>(opt => opt.UseSqlServer(
                Configuration.GetConnectionString("DbLocale")
            ));

            services.AddScoped<InterfaceRepo<Libro>, LibriRepo>();
            services.AddScoped<InterfaceRepo<Categoria>, CategoriaRepo>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("policyLocale");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
