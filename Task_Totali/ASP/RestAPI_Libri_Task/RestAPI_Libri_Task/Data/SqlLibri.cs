﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using RestAPI_Libri_Task.Models;

namespace RestAPI_Libri_Task.Data
{
    public class SqlLibri : InterfaceRepo<Libro>
    {
        private string stringaConnessione = "Server=LAPTOP-V2JOQPIF;Database=Es_Libri;User Id = sharpuser; Password=Cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false";

        public IEnumerable<Libro> GetAll()
        {
            List<Libro> elenco = new List<Libro>();

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT libroID, titolo, descrizione, autore, isbn, quantita FROM Libri";

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Libro temp = new Libro()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Titolo = reader[1].ToString(),
                        Descrizione = reader[2].ToString(),
                        Autore = reader[3].ToString(),
                        Isbn = reader[4].ToString(),
                        Quantita = Convert.ToInt32(reader[5])
                    };

                    elenco.Add(temp);
                }
            }

            return elenco;
        }

        public Libro GetById(int varId)
        {
            Libro temp = null;
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT libroID, titolo, descrizione, autore, isbn, quantita FROM Libri WHERE libroID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);
                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                try
                {
                    reader.Read();
                    temp = new Libro()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Titolo = reader[1].ToString(),
                        Descrizione = reader[2].ToString(),
                        Autore = reader[3].ToString(),
                        Isbn = reader[4].ToString(),
                        Quantita = Convert.ToInt32(reader[5])
                    };
                }
                catch (Exception e)
                {
                }
            }

            return temp;
        }

        public Libro GetByIsbn(string isbn)
        {
            Libro temp = null;
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT libroID, titolo, descrizione, autore, isbn, quantita FROM Libri WHERE isbn = @varIsbn";
                comm.Parameters.AddWithValue("@varIsbn", isbn);
                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                try
                {
                    reader.Read();
                    temp = new Libro()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Titolo = reader[1].ToString(),
                        Descrizione = reader[2].ToString(),
                        Autore = reader[3].ToString(),
                        Isbn = reader[4].ToString(),
                        Quantita = Convert.ToInt32(reader[5])
                    };
                }
                catch (Exception e)
                {
                }
            }

            return temp;
        }

        public bool Insert(Libro obj)
        {
            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "INSERT INTO Libri(titolo, descrizione, autore, isbn, quantita) VALUES (@varTitolo, @varDescrizione, @varAutore, @varIsbn, @varQuantita)";
                comm.Parameters.AddWithValue("@varTitolo", obj.Titolo);
                comm.Parameters.AddWithValue("@varDescrizione", obj.Descrizione);
                comm.Parameters.AddWithValue("@varAutore", obj.Autore);
                comm.Parameters.AddWithValue("@varIsbn", obj.Isbn);
                comm.Parameters.AddWithValue("@varQuantita", obj.Quantita);
                conn.Open();
                try
                {
                    if (comm.ExecuteNonQuery() > 0)
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool Delete(int varId)
        {
            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "DELETE FROM Libri WHERE libroID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);
                conn.Open();
                try
                {
                    if (comm.ExecuteNonQuery() > 0)
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool Update(Libro obj)
        {
            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "UPDATE Libri SET titolo = @varTitolo, descrizione = @varDescrizione, autore = @varAutore, isbn = @varIsbn, quantita = @varQuantita WHERE libroID = @varId";
                comm.Parameters.AddWithValue("@varTitolo", obj.Titolo);
                comm.Parameters.AddWithValue("@varDescrizione", obj.Descrizione);
                comm.Parameters.AddWithValue("@varAutore", obj.Autore);
                comm.Parameters.AddWithValue("@varIsbn", obj.Isbn);
                comm.Parameters.AddWithValue("@varQuantita", obj.Quantita);
                comm.Parameters.AddWithValue("@varId", obj.Id);
                conn.Open();
                try
                {
                    if (comm.ExecuteNonQuery() > 0)
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }
    }
}
