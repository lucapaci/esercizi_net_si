﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestAPI_Libri_Task.Data;
using RestAPI_Libri_Task.Models;

namespace RestAPI_Libri_Task.Controllers
{
    [Route("api/Libri")]
    [ApiController]
    public class LibriController : Controller
    {
        private readonly SqlLibri _repository = new SqlLibri();

        [HttpGet]
        public ActionResult<IEnumerable<Libro>> GetAllLibri() => Ok(_repository.GetAll());

        [HttpGet("isbn/{varIsbn}")]
        public ActionResult<Libro> GetLibroIsbn(string varIsbn) => Ok(_repository.GetByIsbn(varIsbn));

        [HttpGet("{id}")]
        public ActionResult<Libro> GetLibro(int id) => Ok(_repository.GetById(id));

        [HttpPost("insert")]
        public ActionResult InsertLibro(Libro obj)
        {
            if (_repository.Insert(obj))
                return Ok("Successo");

            return Forbid("Errore nell'inserimento");
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteLibro(int id)
        {
            if (_repository.Delete(id))
                return Ok("Successo");
            return Forbid("Errore nell'eliminazione");
        }

        [HttpPut("{id}")]
        public ActionResult UpdateLibro(Libro obj, int id)
        {
            obj.Id = id;
            if (_repository.Update(obj))
                return Ok("Successo");
            return Forbid("Errore nell'aggioramento");
        }
    }
}
