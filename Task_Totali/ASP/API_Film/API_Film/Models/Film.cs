﻿using System;
using System.Collections.Generic;

#nullable disable

namespace API_Film.Models
{
    public partial class Film
    {
        public int FilmId { get; set; }
        public string Titolo { get; set; }
        public string Immagine { get; set; }
        public string Descrizione { get; set; }
        public string Regista { get; set; }
        public string Produttore { get; set; }
        public string DataRilascio { get; set; }
        public int Durata { get; set; }
        public long Incasso { get; set; }
    }
}
