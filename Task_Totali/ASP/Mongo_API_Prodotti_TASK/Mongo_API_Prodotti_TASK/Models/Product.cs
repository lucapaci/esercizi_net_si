﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Mongo_API_Prodotti_TASK.Models
{
    public class Product
    {
        [BsonId]
        public ObjectId DocumentiId { get; set; }
        [Required, MaxLength(255)]
        public string Nome { get; set; }
        [Required, MaxLength(500)]
        public string Descrizione { get; set; }
        [Required]
        public double Prezzo { get; set; }
        [Required]
        public string Sku { get; set; }
        [Required]
        public int Quantita { get; set; }

        public ObjectId Category { get; set; }

        [Required, BsonIgnore]
        public string Categoria { get; set; }

        [BsonIgnore]
        public Category InfoCategory { get; set; }
    }
}
