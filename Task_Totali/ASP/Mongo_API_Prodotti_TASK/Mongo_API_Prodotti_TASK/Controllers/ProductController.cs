﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Mongo_API_Prodotti_TASK.DAL;
using Mongo_API_Prodotti_TASK.Models;
using MongoDB.Bson;

namespace Mongo_API_Prodotti_TASK.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductController : Controller
    {
        private static ProductRepo _repo;

        public ProductController(IConfiguration config)
        {
            if (_repo == null)
            {
                bool isLocale = config.GetValue<bool>("IsLocale");
                string connString = isLocale
                    ? config.GetValue<string>("MongoSettings:DatabaseLocale")
                    : config.GetValue<string>("MongoSettings:DatabaseRemoto");
                string dbName = config.GetValue<string>("MongoSettings:NomeDatabase");
                _repo = new ProductRepo(connString, dbName);
            }
        }

        [HttpGet]
        public ActionResult GetAllProducts() => Ok(_repo.GetAll());
        
        [HttpGet("{varId}")]
        public ActionResult GetProductById(string varId) => Ok(_repo.GetById(ObjectId.Parse(varId)));

        [HttpPost("insert")]
        public ActionResult InsertProduct(Product prod)
        {
            if(_repo.Insert(prod))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

        [HttpDelete("{varId}")]
        public ActionResult DeleteProduct(string varId)
        {
            if(_repo.Delete(ObjectId.Parse(varId)))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyProduct(string varId, Product prod)
        {
            prod.DocumentiId = ObjectId.Parse(varId);
            if(_repo.Update(prod))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

    }
}
