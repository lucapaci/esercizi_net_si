function inserisciLibro() {
  let lbr = {
    nome: $('#inNome').val(),
    descrizione: $('#inDescrizione').val(),
    codice: $('#inCodice').val(),
    quantita: parseInt($('#inQuantita').val()),
  };

  if (
    lbr.nome.length == 0 ||
    lbr.descrizione.length == 0 ||
    lbr.codice.length == 0 ||
    lbr.quantita <= 0
  ) {
    alert('Inserimento non valido');
    return;
  }

  $.ajax({
    url: 'https://localhost:44313/api/Libri/insert',
    method: 'POST',
    data: JSON.stringify(lbr),
    contentType: 'application/json',
    success: (response) => {
      switch (response) {
        case 'success':
          stampaLibri();
          alert('Inserimento completato');
          break;
        case 'error':
          alert('Errore nel salvataggio');
          break;
      }
    },
    error: () => alert('Errore nella richiesta'),
    complete: () => {
      $('#inNome').val('');
      $('#inDescrizione').val('');
      $('#inCodice').val('');
      $('#inQuantita').val('');
    },
  });
}

function stampaLibri() {
  $.ajax({
    url: 'https://localhost:44313/api/Libri',
    method: 'GET',
    success: (response) => {
      libri = response;
      let cont = '';
      for (let item of libri) {
        cont += `<tr>
                    <td>${item.nome}</td>
                    <td>${item.descrizione}</td>
                    <td>${item.codice}</td>
                    <td>${item.quantita}</td>
                    <td>
                        <button type="button" class="btn btn-outline-danger" onclick="eliminaLibro(${item.id})">
                            <i class="fa-solid fa-trash"></i>
                        </button>
                    </td>
                 </tr>`;
      }
      $('#contenutoTabella').html(cont);
    },
  });
}

function eliminaLibro(varId) {
  $.ajax({
    url: `https://localhost:44313/api/Libri/${varId}`,
    method: 'DELETE',
    contentType: 'application/json',
    success: (response) => {
      switch (response) {
        case 'success':
          stampaLibri();
          alert('Eliminazione effettuata');
          break;
        case 'error':
          alert(`Errore nell' eliminazione`);
          break;
      }
    },
    error: () => alert('Errore nella richiesta'),
  });
}

$(document).ready(() => {
  $('#btnInsLib').on('click', () => {
    inserisciLibro();
  });
  stampaLibri();
});

let libri = [];
