function inserisci() {
  let in_titolo = document.getElementById('in_titolo').value;
  let in_autore = document.getElementById('in_autore').value;
  let in_ISBN = document.getElementById('in_ISBN').value;
  let in_prezzo = document.getElementById('in_prezzo').value;
  let in_quant = document.getElementById('in_Quan').value;

  if (
    in_titolo == 0 ||
    in_autore.length == 0 ||
    in_ISBN.length == 0 ||
    isNaN(parseFloat(in_prezzo)) ||
    +in_quant < 0
  ) {
    alert('Inserimento non valido');
    return;
  }

  let libro = {
    id: calcolaId(),
    titolo: in_titolo,
    autore: in_autore,
    isbn: in_ISBN,
    prezzo: parseFloat(in_prezzo),
    quant: parseInt(in_quant),
  };

  if (!isPresente(libro)) {
    elenco.push(libro);
    localStorage.setItem('listaLibri', JSON.stringify(elenco));
  }
  alert('Inserimento completato');
  stampaLista();
}

function newFunction() {
  return document.getElementById;
}

function cancella(varId) {
  for (let [index, item] of elenco.entries()) {
    if (item.id == varId) elenco.splice(index, 1);
  }
  localStorage.setItem('listaLibri', JSON.stringify(elenco));
  stampaLista();
}

function apriModale(varId) {
  for (let item of elenco) {
    if (item.id == varId) {
      document.getElementById('up_id').value = item.id;
      document.getElementById('up_titolo').value = item.titolo;
      document.getElementById('up_autore').value = item.autore;
      document.getElementById('up_ISBN').value = item.isbn;
      document.getElementById('up_prezzo').value = item.prezzo;
      document.getElementById('up_Quan').value = item.quant;
    }
  }
  $('#modaleModifica').modal('show');
}

function aggiorna() {
  let up_id = document.getElementById('up_id').value;
  let up_titolo = document.getElementById('up_titolo').value;
  let up_autore = document.getElementById('up_autore').value;
  let up_ISBN = document.getElementById('up_ISBN').value;
  let up_prezzo = document.getElementById('up_prezzo').value;
  let up_Quan = document.getElementById('up_Quan').value;

  let libNuovo = {
    id: up_id,
    titolo: up_titolo,
    autore: up_autore,
    isbn: up_ISBN,
    prezzo: parseFloat(up_prezzo),
    quant: parseInt(up_Quan),
  };

  for (let item of elenco) {
    if (item.id == libNuovo.id) {
      item.titolo = libNuovo.titolo;
      item.autore = libNuovo.autore;
      item.isbn = libNuovo.isbn;
      item.prezzo = libNuovo.prezzo;
      item.quant = libNuovo.quant;
    }
  }
  $('#modaleModifica').modal('hide');
  localStorage.setItem('listaLibri', JSON.stringify(elenco));
  stampaLista();
}

function ricerca() {
  if (elenco.length != 0) {
    document.getElementById('contenutoTabella').innerHTML = '';
    let ric = document.getElementById('in_ricerca').value;
    let content = '';
    let counter = 0;
    for (let item of elenco) {
      if (item.titolo.toLowerCase().includes(ric.toLowerCase())) {
        counter += item.prezzo * item.quant;
        let elemento = `<tr>
                            <td>${item.titolo}</td>
                            <td>${item.autore}</td>
                            <td>${item.isbn}</td>
                            <td>${item.prezzo}</td>
                            <td>${item.quant}</td>
                            <td>
                                <button type="button" class="btn btn-outline-danger btn-block" onclick="cancella(${item.id})">
                                    <i class="fa-solid fa-trash"></i>   
                                </button>
                                <button type="button" class="btn btn-outline-warning btn-block" onclick="apriModale(${item.id})">
                                    <i class="fa-solid fa-pencil"></i>
                                </button>
                            </td>
                        </tr>`;
        content += elemento;
      }
    }
    if (counter > 0) {
      let rigaSomma = `<tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Prezzo Totale:</td>
                            <td>${counter}&euro;</td>
                       </tr>`;
      content += rigaSomma;
    }
    document.getElementById('contenutoTabella').innerHTML = content;
  }
}

function stampaLista() {
  if (elenco.length != 0) {
    document.getElementById('contenutoTabella').innerHTML = '';
    let content = '';
    let counter = 0;
    for (let item of elenco) {
      counter += item.prezzo * item.quant;
      let elemento = `<tr>
                              <td>${item.titolo}</td>
                              <td>${item.autore}</td>
                              <td>${item.isbn}</td>
                              <td>${item.prezzo}</td>
                              <td>${item.quant}</td>
                              <td>
                              <button type="button" class="btn btn-outline-danger btn-block" onclick="cancella(${item.id})">
                                  <i class="fa-solid fa-trash"></i>   
                              </button>
                              <button type="button" class="btn btn-outline-warning btn-block" onclick="apriModale(${item.id})">
                                  <i class="fa-solid fa-pencil"></i>
                              </button>
                              </td>
                           </tr>`;
      content += elemento;
    }
    if (counter > 0) {
      let rigaSomma = `<tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Prezzo Totale:</td>
                            <td>${counter}&euro;</td>
                       </tr>`;
      content += rigaSomma;
    }
    document.getElementById('contenutoTabella').innerHTML = content;
  } else {
    document.getElementById('contenutoTabella').innerHTML = '';
  }
}

function isPresente(libro) {
  for (let item of elenco) {
    if (
      item.isbn == libro.isbn &&
      item.titolo.toLowerCase() == libro.titolo.toLowerCase()
    ) {
      item.quant += libro.quant;
      return true;
    }
  }
  return false;
}

function calcolaId() {
  if (elenco.length != 0) return elenco[elenco.length - 1].id + 1;

  return 1;
}

if (localStorage.getItem('listaLibri') == null)
  localStorage.setItem('listaLibri', JSON.stringify([]));

let elenco = JSON.parse(localStorage.getItem('listaLibri'));
stampaLista();
