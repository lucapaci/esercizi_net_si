function printCategories() {
  fetch('https://localhost:44357/api/categories')
    .then((resp) => resp.json())
    .then((resp) => {
      let itemList = '';
      resp.forEach((i) => {
        itemList += `<option value="${i.titolo}">${i.titolo}</option>`;
      });
      $('#inCategoria').html(itemList);

      let cont = '';
      resp.forEach((i) => {
        cont += `<tr>
                    <td>${i.titolo}</td>
                    <td>${i.scaffale}</td>
                    <td>${i.descrizione}</td>
                </tr>`;
      });
      $('#cateTableCont').html(cont);
    })
    .catch((err) => console.log(err));
}

function insertCategory() {
  let cat = {
    titolo: $('#inTitolo').val(),
    scaffale: $('#inScaffale').val(),
    descrizione: $('#inDescrizione').val(),
  };

  if (
    cat.titolo.length == 0 ||
    cat.scaffale.length == 0 ||
    cat.descrizione.length == 0
  ) {
    alert('Parametri non validi');
    return;
  }

  fetch('https://localhost:44357/api/categories/insert', {
    method: 'POST',
    body: JSON.stringify(cat),
    headers: { 'Content-type': 'application/json; charset=UTF-8' },
  })
    .then((resp) => resp.json())
    .then((resp) => {
      switch (resp.status) {
        case 'success':
          alert('inserimento andato a buon fine');
          printCategories();
          break;
        case 'error':
          alert('categoria gia presente');
          break;
      }
    })
    .catch(() => alert('Problemi nella richiesta'));
}

function printProducts() {
  fetch('https://localhost:44357/api/products')
    .then((resp) => resp.json())
    .then((resp) => {
      let tableCont = '';
      resp.forEach((i) => {
        tableCont += `<tr>
                    <td>${i.nome}</td>
                    <td>${i.descrizione}</td>
                    <td>${i.prezzo}&euro;</td>
                    <td>${i.sku}</td>
                    <td>${i.quantita}</td>
                    <td>${i.infoCategory.titolo}</td>
                </tr>`;
      });
      $('#prodTableCont').html(tableCont);
    })
    .catch((err) => console.log(err));
}

function insertProduct() {
  let prod = {
    nome: $('#inNome').val(),
    descrizione: $('#inDescrizione').val(),
    prezzo: parseFloat($('#inPrezzo').val()),
    sku: $('#inSku').val(),
    quantita: parseInt($('#inQuantita').val()),
    categoria: $('#inCategoria').val(),
  };

  if (
    prod.nome.length == 0 ||
    prod.descrizione.length == 0 ||
    isNaN(prod.prezzo) ||
    prod.sku.length == 0 ||
    isNaN(prod.quantita) ||
    prod.categoria.length == 0
  ) {
    alert('Parametri non validi');
    return;
  }

  fetch('https://localhost:44357/api/products/insert', {
    method: 'POST',
    body: JSON.stringify(prod),
    headers: { 'Content-type': 'application/json; charset=UTF-8' },
  })
    .then((resp) => resp.json())
    .then((resp) => {
      switch (resp.status) {
        case 'success':
          alert('inserimento andato a buon fine');
          printProducts();
          break;
        case 'error':
          alert('prodotto gia presente');
          break;
      }
    })
    .catch(() => alert('Problemi nella richiesta'));
}

$(document).ready(() => {
  $('#btnInsCate').click(insertCategory);
  $('#btnInsProd').click(insertProduct);
});

printCategories();
printProducts();
