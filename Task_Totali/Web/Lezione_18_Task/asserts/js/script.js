let elenco = [];

function inserisci() {
  let inNome = document.getElementById('inNome').value;
  let inCognome = document.getElementById('inCognome').value;
  let inSesso = document.getElementById('inSesso').value;
  let inMatricola = document.getElementById('inMatricola').value;
  let inAnno = document.getElementById('inAnno').value;

  if (inMatricola.length == 0) {
    alert('matricola obbligatoria');
    return;
  }

  let persona = {
    nome: inNome,
    cognome: inCognome,
    sesso: inSesso,
    matricola: inMatricola,
    dataIscr: inAnno,
  };

  if (!controlloMatricola(persona.matricola)) elenco.push(persona);
  else alert('Matricola gia presente');

  stampa();
  document.getElementById('inNome').value = '';
  document.getElementById('inCognome').value = '';
  document.getElementById('inMatricola').value = '';
  document.getElementById('inAnno').value = '';
  document.getElementById('inNome').focus();
}

function stampa() {
  document.getElementById('contenutoTabella').innerHTML = '';
  let content = '';
  for (let item of elenco) {
    let elemento = `<tr ${coloreAnno(item.dataIscr)}>
                      <td>${item.nome}</td>
                      <td>${item.cognome}</td>
                      <td>${item.sesso}</td>
                      <td>${item.matricola}</td>
                      <td>${item.dataIscr}</td>
                    </tr>`;
    content += elemento;
  }
  document.getElementById('contenutoTabella').innerHTML += content;
}

function controlloMatricola(varMatricola) {
  for (let item of elenco) {
    if (item.matricola == varMatricola) return true;
  }

  return false;
}

function cancellaTutto() {
  elenco = [];
  document.getElementById('contenutoTabella').innerHTML = '';
  document.getElementById('inNome').focus();
}

function coloreAnno(dataIscrizione) {
  let numeroAnni =
    new Date().getFullYear() - new Date(dataIscrizione).getFullYear();

  if (numeroAnni > 5 && numeroAnni < 0) return `class="text-danger"`;
  if (numeroAnni >= 3 && numeroAnni <= 5) return `class="text-success"`;
  if (numeroAnni < 3) return `class="text-primary"`;
}
