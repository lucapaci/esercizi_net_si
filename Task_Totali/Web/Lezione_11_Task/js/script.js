function iscriviti() {
  let inNome = document.getElementById('inNome').value;
  let inCognome = document.getElementById('inCognome').value;
  let inDataNascita = document.getElementById('inDataNascita').value;
  let inCorso = document.getElementById('inCorso').value;

  let persona = {
    Nome: inNome,
    Cognome: inCognome,
    DataDiNascita: inDataNascita,
    Corso: inCorso,
  };

  if (
    inNome.length >= 3 &&
    inCognome.length >= 3 &&
    inDataNascita.length >= 3 &&
    inCorso.length >= 3
  ) {
    alert(
      `Nome: ${persona.Nome}\nCognome: ${persona.Cognome}\nData: ${persona.DataDiNascita}\nCorso: ${persona.Corso}`
    );
  } else {
    alert('Compilare correttamente i dati');
  }
}
