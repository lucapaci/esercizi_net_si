function stampaCategoria(prodotti, varCategoria, idTabella) {
  let cont = '';
  prodotti.forEach((i) => {
    if (i.categoria.toLowerCase() === varCategoria.toLowerCase()) {
      let elem = `<tr>
          <td>${i.genere}</td>
          <td>${i.prodotto}</td>
          <td>${i.prezzo}</td>
          <td>${i.venduti}</td>
          </tr>`;
      cont += elem;
    }
  });
  $(`#${idTabella}`).html(cont);
}

function stampaGenere(prodotti, genere, idTabella) {
  let cont = '';
  prodotti.forEach((i) => {
    if (i.genere.toLowerCase() === genere.toLowerCase()) {
      let elem = `<tr>
            <td>${i.categoria}</td>
            <td>${i.prodotto}</td>
            <td>${i.prezzo}</td>
            <td>${i.venduti}</td>
            </tr>`;
      cont += elem;
    }
  });
  $(`#${idTabella}`).html(cont);
}

function stampaSottoTrenta(prodotti) {
  let cont = '';
  prodotti.forEach((i) => {
    if (parseInt(i.prezzo) < 30) {
      let elem = `<tr>
            <td>${i.genere}</td>
            <td>${i.prodotto}</td>
            <td>${i.prezzo}</td>
            <td>${i.venduti}</td>
            </tr>`;
      cont += elem;
    }
  });
  $(`#contSottoTrenta`).html(cont);
}

function numeroCategorie(prodotti) {
  let noDoppioni = [];
  for (let i of prodotti) {
    let flag = false;
    for (let item of noDoppioni) {
      if (i.categoria == item) flag = true;
    }
    if (!flag) noDoppioni.push(i.categoria);
  }
  return noDoppioni.length;
}

function stampaTopCinque(prodotti) {
  let TopVendite = prodotti.sort(comparePrezzi).reverse().slice(0, 5);
  let cont = '';
  TopVendite.forEach((i) => {
    let elem = `<tr>
            <td>${i.genere}</td>
            <td>${i.prodotto}</td>
            <td>${i.prezzo}</td>
            <td>${i.venduti}</td>
            </tr>`;
    cont += elem;
  });
  $(`#contTopCinq`).html(cont);
}

function stampaTabelle(prodotti) {
  stampaCategoria(prodotti, 'abbigliamento', 'contAbbigl');
  stampaCategoria(prodotti, 'calzature', 'contCalz');
  stampaCategoria(prodotti, 'borse', 'contBorse');
  stampaGenere(prodotti, 'uomo', 'contUomo');
  stampaGenere(prodotti, 'donna', 'contDonna');
  stampaSottoTrenta(prodotti);
  stampaTopCinque(prodotti);
  $('#numCat').html(numeroCategorie(prodotti));
}

function comparePrezzi(a, b) {
  if (a.venduti < b.venduti) return -1;
  if (a.venduti > b.venduti) return 1;
  return 0;
}

function prendiProdotti() {
  $.ajax({
    url: 'http://www.giovannimastromatteo.it/projects/prodotti.json',
    method: 'GET',
    success: (resp) => {
      stampaTabelle(resp);
    },
    error: () => console.log('Errore'),
  });
}

$(document).ready(() => {
  prendiProdotti();
});
