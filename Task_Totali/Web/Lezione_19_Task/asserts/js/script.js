function inserisci() {
  let inAlimento = document.getElementById('inAlimento').value;
  let inQuantita = document.getElementById('inQuantita').value;

  if (inAlimento.length == 0) return;

  let cibo = {
    nome: inAlimento,
    quantita: parseInt(inQuantita),
  };

  let indice = findCibo(cibo.nome);

  if (indice == -1) {
    lista.push(cibo);
  } else {
    lista[indice].quantita += +inQuantita;
  }

  stampa();

  localStorage.setItem('lista', JSON.stringify(lista));
}

function cancella(varNome) {
  lista.splice(findCibo(varNome), 1);
  localStorage.setItem('lista', JSON.stringify(lista));
  stampa();
}

function aggiungiUno(varNome) {
  lista[findCibo(varNome)].quantita += +1;
  localStorage.setItem('lista', JSON.stringify(lista));
  stampa();
}

function stampa() {
  document.getElementById('contenutoTabella').innerHTML = '';
  let content = '';
  for (let item of lista) {
    let element = `<tr>
                        <td>${item.nome}</td>
                        <td>${item.quantita}</td>
                        <td>
                            <button type="button" class="btn btn-primary fa-solid fa-plus" onclick="aggiungiUno('${item.nome}')"></button>
                            <button type="button" class="btn btn-danger fa-solid fa-trash-can" onclick="cancella('${item.nome}')"/>
                        </td>
                    </tr>`;
    content += element;
  }
  document.getElementById('contenutoTabella').innerHTML = content;
}

function findCibo(varNome) {
  for (let [index, item] of lista.entries()) {
    if (item.nome == varNome) return index;
  }
  return -1;
}

if (localStorage.getItem('lista') == null)
  localStorage.setItem('lista', JSON.stringify([]));

let lista = JSON.parse(localStorage.getItem('lista'));
stampa();
