function stampaChat() {
  $.ajax({
    url: 'https://localhost:44305/api/messaggi',
    method: 'GET',
    success: (response) => {
      chat = response;
      let cont = '';
      for (let item of chat) {
        cont += `<div class="alert alert-info" role="alert">
                  <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">${item.nickname}</h5>
                    <small class="text-muted">${item.orario.slice(-7)}</small>
                  </div>
                  <p class="mb-1">${item.contenuto}</p>
                </div>`;
      }
      $('#chatCont').html(cont);
    },
    error: (err) => console.log(err),
  });
}

function salvaMessaggio() {
  let msgText = $('#inMsg').val();
  if (msgText.length != 0) {
    let message = {
      nickname: nick,
      contenuto: msgText,
    };

    $.ajax({
      url: 'https://localhost:44305/api/messaggi/insert',
      method: 'POST',
      data: JSON.stringify(message),
      contentType: 'application/json',
      success: (resp) => {
        console.log(resp);
      },
      error: (err) => console.log(err),
      complete: () => $('#inMsg').val(''),
    });
  }
}

let interval;

$(document).ready(function () {
  interval = setInterval(() => stampaChat(), 500);
  $('#inNick').val(nick);
  $('#nickPlace').html('&nbsp;' + nick);
});

function salvaNick() {
  let inNick = $('#inNick').val();
  if (inNick.length != 0) {
    nick = inNick;
    localStorage.setItem('lastNick', JSON.stringify(nick));
    window.location.href = 'chat.html';
  }
}

function logOut() {
  window.location.href = 'login.html';
}

if (localStorage.getItem('lastNick') == null)
  localStorage.setItem('lastNick', JSON.stringify(''));

let nick = '';
nick = JSON.parse(localStorage.getItem('lastNick'));

let chat = [];
