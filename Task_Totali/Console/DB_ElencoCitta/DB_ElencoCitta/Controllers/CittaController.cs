﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DB_ElencoCitta.DAL;
using DB_ElencoCitta.Models;
using Microsoft.Extensions.Configuration;

namespace DB_ElencoCitta.Controllers
{
    public class CittaController
    {
        public static IConfiguration configurazione;

        public CittaController()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            configurazione = builder.Build();
        }

        public void StampaCitta()
        {
            CittaDAL cittaDal = new CittaDAL(configurazione);

            List<Citta> elenco = cittaDal.GetListaCitta();

            foreach (Citta item in elenco)
            {
                Console.WriteLine(item);
            }
        }

        public void InserireCitta(string varNome, string varProvincia)
        {
            Citta temp = new Citta()
            {
                Nome = varNome,
                Provincia = varProvincia
            };

            CittaDAL cittaDal = new CittaDAL(configurazione);
            if (cittaDal.InsertCitta(temp))
                Console.WriteLine("Tutto OK!");
            else
                Console.WriteLine("Sè rotto tutto");
        }

        public void EliminaCittaPerId(int varId)
        {
            CittaDAL cittaDal = new CittaDAL(configurazione);
            if (cittaDal.DeleteCitta(varId))
                Console.WriteLine("Tutto OK!");
            else
                Console.WriteLine("Sè rotto tutto");
        }

        public void ModificaCittaPerId(int varId, string varNome, string varProvincia)
        {
            Citta temp = new Citta()
            {
                Id = varId,
                Nome = varNome,
                Provincia = varProvincia
            };

            CittaDAL cittaDal = new CittaDAL(configurazione);
            if (cittaDal.UpdateCitta(temp))
                Console.WriteLine("Tutto OK!");
            else
                Console.WriteLine("Sè rotto tutto");
        }
    }
}
