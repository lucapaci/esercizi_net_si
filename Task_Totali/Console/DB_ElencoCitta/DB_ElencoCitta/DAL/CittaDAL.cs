﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DB_ElencoCitta.Models;
using Microsoft.Extensions.Configuration;

namespace DB_ElencoCitta.DAL
{
    public class CittaDAL
    {
        private string stringaConnessione;

        public CittaDAL(IConfiguration config)
        {
            stringaConnessione = config.GetConnectionString("ServerLocale");
        }

        public List<Citta> GetListaCitta()
        {
            List<Citta> elencoTemp = new List<Citta>();

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                string query = "SELECT cittaID, nome, prov FROM Citta";
                SqlCommand comm = new SqlCommand(query, connessione);
                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Citta temp = new Citta()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Provincia = reader[2].ToString()
                    };
                    elencoTemp.Add(temp);
                }
            }
            return elencoTemp;
        }

        public bool InsertCitta(Citta objCitta)
        {
            bool risultato = false;

            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = $"INSERT INTO Citta(nome, prov) VALUES(@nomeCitta, @provCitta)";
                comm.Parameters.AddWithValue("@nomeCitta", objCitta.Nome);
                comm.Parameters.AddWithValue("@provCitta", objCitta.Provincia);
                conn.Open();

                if (comm.ExecuteNonQuery() > 0)
                    risultato = true;
            }
            return risultato;
        }

        public bool DeleteCitta(int varId)
        {
            bool risultato = false;

            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "DELETE FROM Citta WHERE cittaID = @idCitta";
                comm.Parameters.AddWithValue("@idCitta", varId);

                conn.Open();
                if (comm.ExecuteNonQuery() > 0)
                    risultato = true;
            }
            return risultato;
        }

        public bool UpdateCitta(Citta objCitta)
        {
            bool risultato = false;

            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "UPDATE Citta SET nome = @nomeCitta, prov = @varProvincia WHERE cittaID = @varId";
                comm.Parameters.AddWithValue("@nomeCitta", objCitta.Nome);
                comm.Parameters.AddWithValue("@varProvincia", objCitta.Provincia);
                comm.Parameters.AddWithValue("@varId", objCitta.Id);

                conn.Open();
                if (comm.ExecuteNonQuery() > 0)
                    risultato = true;
            }
            return risultato;
        }
    }
}
