﻿using System;
using es_10.Classes;

namespace es_10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Quale tipologia vuoi creare: Automobile o Moto?");
            string tipo = Console.ReadLine().ToLower().Trim();

            if (!tipo.Equals("automobile") || 
                !tipo.Equals("automobile") ||
                string.IsNullOrEmpty(tipo))
            {
                Console.WriteLine("Valore non valido");
                Environment.Exit(1);
            }

            Console.WriteLine("Inserire marca:");
            string marca = Console.ReadLine();
            Console.WriteLine("Inserire modello:");
            string modello = Console.ReadLine();
            Console.WriteLine("Inserire colore:");
            string colore = Console.ReadLine();
            Console.WriteLine("Inserire numero di ruote:");
            int numRuote = Convert.ToInt32(Console.ReadLine()); 
            Console.WriteLine("Inserire cilindrata:");
            int cilindrata = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Inserire posti:");
            int numPosti = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Inserire numero di marce:");
            int numMarce = Convert.ToInt32(Console.ReadLine());

            if (tipo.ToLower().Equals("automobile"))
            {
                Automobile UserCar = new Automobile(marca, modello, colore, numRuote, cilindrata, numPosti, numMarce);
            }
        }
    }
}
