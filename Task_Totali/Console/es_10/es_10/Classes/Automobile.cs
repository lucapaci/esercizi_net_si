﻿using System;
using System.Collections.Generic;
using System.Text;

namespace es_10.Classes
{
    public class Automobile : Veicolo
    {
        public string Marca { get; set; }
        public string Modello { get; set; }
        public int NumMarce { get; set; }

        public Automobile()
        {
            
        }

        public Automobile(string marca, string modello, string colore, int numRuote, int cilindrata, int posti, int numMarce)
        {
            Marca = marca;
            Modello = modello;
            Colore = colore;
            NumeroRuote = numRuote;
            Cilindrata = cilindrata;
            Posti = posti;
            NumMarce = numMarce;
        }

        public override void Azione()
        {
            Console.WriteLine("Accellera");
        }
    }
}
