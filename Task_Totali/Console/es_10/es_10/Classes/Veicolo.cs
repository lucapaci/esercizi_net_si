﻿using System;
using System.Collections.Generic;
using System.Text;

namespace es_10.Classes
{
    public abstract class Veicolo
    {
        public string Colore { get; set; }

        public int NumeroRuote { get; set; }

        public int Cilindrata { get; set; }

        public int Posti { get; set; }

        public abstract void Azione();
    }
}
