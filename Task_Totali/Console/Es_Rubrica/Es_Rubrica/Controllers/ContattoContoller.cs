﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Channels;
using Es_Rubrica.DAL;
using Es_Rubrica.Models;
using Microsoft.Extensions.Configuration;

namespace Es_Rubrica.Controllers
{
    public class ContattoContoller
    {
        public static IConfiguration configurazione;

        public ContattoContoller()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            configurazione = builder.Build();
        }

        public void StampaContatti()
        {
            ContattoDAL contDal = new ContattoDAL(configurazione);

            List<Contatto> elenco = contDal.GetListaContatti();

            elenco.ForEach(Console.WriteLine);
        }

        public void StampaContatto(int varId)
        {
            ContattoDAL contDal = new ContattoDAL(configurazione);

            Contatto contatto = contDal.GetContattoPerID(varId);
            Console.WriteLine(contatto);
        }

        public void InserisciContatto(string varNome, string varCognome, string varTelefono)
        {
            ContattoDAL contDal = new ContattoDAL(configurazione);
            if (contDal.GetContattoPerTelefono(varTelefono) != null)
            {
                Console.WriteLine("Contatto gia presente in Rubrica");
            }
            else
            {
                Contatto temp = new Contatto()
                {
                    Nome = varNome,
                    Cognome = varCognome,
                    Telefono = varTelefono
                };

                if(contDal.InsertContatto(temp))
                    Console.WriteLine("Inserimento avvenuto correttamente");
                else 
                    Console.WriteLine("Errore nell'inserimento");
            }
        }

        public void CancellaContatto(int varId)
        {
            ContattoDAL contDal = new ContattoDAL(configurazione);
            if (contDal.DeleteContatto(varId))
                Console.WriteLine("Contatto elimitato correttamente");
            else
                Console.WriteLine("Si è rotto tutto");    
        }

        public void AggiornaContatto(int varId, string varNome, string varCognome, string varTelefono)
        {
            ContattoDAL contDal = new ContattoDAL(configurazione);
            Contatto temp = new Contatto()
            {
                Id = varId,
                Nome = varNome,
                Cognome = varCognome,
                Telefono = varTelefono
            };

            if (contDal.UpdateContatto(temp))
                Console.WriteLine("L'aggiornamento è andato a buon fine");
            else
                Console.WriteLine("Si è rotto tutto");
        }
    }
}
