﻿using System;
using Es_Rubrica.Controllers;

namespace Es_Rubrica
{
    class Program
    {
        static void Main(string[] args)
        {
            ContattoContoller gestore = new ContattoContoller();
            //gestore.InserisciContatto("Tony", "elle", "122456799");
            gestore.StampaContatti();
            //gestore.AggiornaContatto(5, "Geno", "Veffa", "122456799");
            gestore.CancellaContatto(5);
            gestore.StampaContatti();
        }
    }
}
