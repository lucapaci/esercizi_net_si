﻿using System;
using System.Collections.Generic;
using System.Text;
using Es_Rubrica.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Es_Rubrica.DAL
{
    public class ContattoDAL
    {
        private string connectionString;

        public ContattoDAL(IConfiguration config)
        {
            connectionString = config.GetConnectionString("Rubrica");
        }

        public List<Contatto> GetListaContatti()
        {
            List<Contatto> elencoTemp = new List<Contatto>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "SELECT ContattoID, nome, cognome, telefono FROM Contatti";

                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    Contatto contTemp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };
                    elencoTemp.Add(contTemp);
                }
            }
            return elencoTemp;
        }

        public Contatto GetContattoPerID(int varId)
        {
            Contatto cont = null;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "SELECT ContattoID, nome, cognome, telefono FROM Contatti WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);
                    
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                reader.Read();
                cont = new Contatto()
                {
                    Id = Convert.ToInt32(reader[0]),
                    Nome = reader[1].ToString(),
                    Cognome = reader[2].ToString(),
                    Telefono = reader[3].ToString()
                };
            }
            return cont;
        }        
        
        public Contatto GetContattoPerTelefono(string varTelefono)
        {
            Contatto cont = null;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "SELECT ContattoID, nome, cognome, telefono FROM Contatti WHERE telefono = @varTelefono";
                comm.Parameters.AddWithValue("@varTelefono", varTelefono);
                    
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.HasRows)
                {
                    cont = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };
                }
            }
            return cont;
        }

        public bool InsertContatto(Contatto objContatto)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "INSERT INTO Contatti(nome, cognome, telefono) VALUES (@varNome, @varCognome, @varTelefono)";
                comm.Parameters.AddWithValue("@varNome", objContatto.Nome);
                comm.Parameters.AddWithValue("@varCognome", objContatto.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", objContatto.Telefono);
                conn.Open();

                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }

        public bool DeleteContatto(int varId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "DELETE FROM Contatti WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);
                conn.Open();

                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }

        public bool UpdateContatto(Contatto objContatto)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "UPDATE Contatti SET nome = @varNome, cognome = @varCognome, telefono = @varTelefono WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varNome", objContatto.Nome);
                comm.Parameters.AddWithValue("@varCognome", objContatto.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", objContatto.Telefono);
                comm.Parameters.AddWithValue("@varId", objContatto.Id);
                conn.Open();

                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }
    }
}
