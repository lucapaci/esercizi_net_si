﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Channels;

namespace es_13.Classes
{
    public class Stanza
    {
        public string Nome { get; set; }
        private List<Oggetto> oggetti;

        public List<Oggetto> Oggetti
        {
            get => oggetti;
            set => oggetti = value;
        }

        public Stanza()
        {
            Oggetti = new List<Oggetto>();
        }

        public Stanza(string nome)
        {
            Oggetti = new List<Oggetto>();
            Nome = nome;
        }

        public void StampaOggetti()
        {
            Console.WriteLine($"-------{Nome}:");
            Oggetti.ForEach(Console.WriteLine);
            Console.WriteLine("-------");
        }
    }
}
