﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace es_13.Classes
{
    public class Oggetto
    {
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public int Valore { get; set; }

        public Oggetto()
        {
            
        }

        public Oggetto(string nome, string descrizione, int valore)
        {
            Nome = nome;
            Descrizione = descrizione;
            Valore = valore;
        }

        public override string ToString()
        {
            return $"Nome: {Nome}\n" +
                   $"Descrizione: {Descrizione}\n" +
                   $"Valore: {Valore}";
        }
    }
}
