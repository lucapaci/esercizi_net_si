﻿using System;
using es_13.Classes;

namespace es_13
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Stanza stanza = new Stanza("Camera");

            stanza.Oggetti.Add(new Oggetto()
            {
                Nome = "Penna",
                Descrizione = "Serve a scrivere",
                Valore = 2
            });

            stanza.Oggetti.Add(new Oggetto()
            {
                Nome = "Mouse",
                Descrizione = "Periferica del computer",
                Valore = 1
            });

            stanza.StampaOggetti();

        }
    }
}
