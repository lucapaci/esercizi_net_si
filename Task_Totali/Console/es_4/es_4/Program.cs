﻿using System;

namespace es_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("USA?: Y/N");
            string nazionalita = Console.ReadLine();

            Console.WriteLine("Inserire giorno: ");
            int num = Convert.ToInt32(Console.ReadLine());

            if (nazionalita.Equals("Y"))
            {
                if (num == 1) num = 7;
                else if (num > 1 && num <= 7) num--;
            }


            switch (num)
            {
                case 1:
                    Console.WriteLine("Lunedì");
                    break;
                case 2:
                    Console.WriteLine("Martedì");
                    break;
                case 3:
                    Console.WriteLine("Mercoledì");
                    break;
                case 4:
                    Console.WriteLine("Giovedì");
                    break;
                case 5:
                    Console.WriteLine("Venerdì");
                    break;
                case 6:
                    Console.WriteLine("Sabato");
                    break;
                case 7:
                    Console.WriteLine("Domenica");
                    break;
                default:
                    Console.WriteLine("Inserire un numero da 1 - 7");
                    break;
            }

            

        }
    }
}
