﻿using System;

namespace es_11.Classes
{
    public class CartaDiIdentita
    {
        public string Codice { get; set; }
        public string DataDiEmissione { get; set; }
        public string DataDiScadenza { get; set; }
        private string _emissione;
        public string Emissione
        {
            get => _emissione;
            set
            {
                if (value.Trim().ToLower().Equals("comune") ||
                    value.Trim().ToLower().Equals("zecca"))
                    _emissione = value;
                else
                    Console.WriteLine("[ERROR] Parametro non consentito, Scegliere tra: (comune)/(zecca)");
            }
        }

        public CartaDiIdentita() {}

        public CartaDiIdentita(string codice, string dataDiEmissione,string dataDiScadenza, string emissione)
        {
            Codice = codice;
            DataDiEmissione = dataDiEmissione;
            DataDiScadenza = dataDiScadenza;
            Emissione = emissione;
        }

        public override string ToString()
        {
            return
                $"CI: {Codice} Data emissione: {DataDiEmissione} Data scadenza: {DataDiScadenza} Ente di emissione: {Emissione}";
        }
    }

}