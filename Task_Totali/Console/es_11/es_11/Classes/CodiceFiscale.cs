﻿namespace es_11.Classes
{
    public class CodiceFiscale
    {
        public string Codice { get; set; }
        public string DataDiScadenza { get; set; }

        public CodiceFiscale() {}

        public CodiceFiscale(string codice, string dataDiScadenza)
        {
            Codice = codice;
            DataDiScadenza = dataDiScadenza;
        }

        public override string ToString()
        {
            return $"CF: {Codice} Expire: {DataDiScadenza}";
        }
    }
}