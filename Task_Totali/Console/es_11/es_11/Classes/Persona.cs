﻿using System;
using System.Collections.Generic;
using System.Text;

namespace es_11.Classes
{
    public class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }

        public CodiceFiscale CodFis { get; set; }
        public CartaDiIdentita CartaIdentita { get; set; }

        public Persona(){}

        public Persona(string nome, string cognome, CodiceFiscale codiceFiscale, CartaDiIdentita cartaDiIdentita)
        {
            Nome = nome;
            Cognome = cognome;
            CodFis = codiceFiscale;
            CartaIdentita = cartaDiIdentita;
        }

        public override string ToString()
        {
            return $"Nome: {Nome} Cognome: {Cognome}\n{CartaIdentita}\n{CodFis}";
        }
    }
}
