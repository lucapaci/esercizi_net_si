﻿using System;
using es_11.Classes;

namespace es_11
{
    class Program
    {
        static void Main(string[] args)
        {
                /*Creare un sistema in grado di immagazinare i dati relativi ad una persona.

                * Inoltre, sarà necessario immagazinare, all'interno di una persona, i dati 
                * relativi a:
                *-Codice Fiscale
                * | -CODICE
                * | Data di scadenza
                *
                *-Carta di Identita: 
                * | -CODICE
                * | -Data di Emissione
                * | -Data di Scadenza
                * | Emissione(comune, zecca dello stato)
                */
            Persona luca = new Persona()
            {
                Nome = "Luca",
                Cognome = "Pacifici",
                CodFis = new CodiceFiscale()
                {
                    Codice = "nsdjansjdns",
                    DataDiScadenza = "13/32/2123"
                },
                CartaIdentita = new CartaDiIdentita()
                {
                    Codice = "asadasdasdasdas32",
                    DataDiEmissione = "23/12/1312",
                    DataDiScadenza = "23/12/1312",
                    Emissione = "Comune"
                }
            };

            Console.WriteLine(luca);
        }
    }
}
