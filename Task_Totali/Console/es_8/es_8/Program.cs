﻿using System;

namespace es_8
{
    class Program
    {
        static string ControlloTemp(double temp)
        {
            if (temp <= 35 || temp > 42)
                return "Deceduto";
            else if (temp <= 37.5)
                return "Accesso consentito";
            else
                return "Accesso negato";
        }

        static void Main(string[] args)
        {
            Console.Write("Inserire la temperatura:");
            double temperatura = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"L'esito del cliente: {ControlloTemp(temperatura)}");
        }
    }
}
