﻿using System;
using es_9.classes;

namespace es_9
{
    class Program
    {
        static void Main(string[] args)
        {
            Studente stud1 = new Studente("Luca", "Pacifici", "Cosdsa", "Ingegneria");
            Studente stud2 = new Studente("Luca", "Pacifici", "Cosdsa", "Ingegneria");
            Studente stud3 = new Studente("Luca", "Pacifici", "Cosdsa", "Ingegneria");
            Studente stud4 = new Studente("Luca", "Pacifici", "Cosdsa", "Ingegneria");

            Console.WriteLine(stud1);
            Console.WriteLine(stud2);
            Console.WriteLine(stud3);
            Console.WriteLine(stud4);
        }
    }
}
