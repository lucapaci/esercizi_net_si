﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Text;
using System.Threading.Channels;

namespace es_9.classes
{
    class Studente : Persona
    {
        private static int _counter;
        public int Matricola { get; set; }
        public String CorsoDiStudi { get; set; }

        public Studente()
        {
            Matricola = ++_counter;
        }

        public Studente(string nome, string cognome, string codFis, string corsoDiStudi)
        {
            Nome = nome;
            Cognome = cognome;
            CodFis = codFis;
            CorsoDiStudi = corsoDiStudi;
            Matricola = ++_counter;
        }

        public override string ToString()
        {
            return
                $"Nome: {Nome} Cognome: {Cognome} CodFis:{CodFis} CorsoDiStudi:{CorsoDiStudi} Matricola: {Matricola}";
        }
    }
}
