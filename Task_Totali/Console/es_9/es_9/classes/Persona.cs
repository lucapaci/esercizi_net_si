﻿using System;
using System.Collections.Generic;
using System.Text;

namespace es_9.classes
{
    class Persona
    {
        public String Nome { get; set; }
        public String Cognome { get; set; }
        public String CodFis { get; set; }
    }
}
